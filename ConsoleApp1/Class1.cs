﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Note
    {

        public string first_name;
        public string last_name;
        public string patronymic;
        public long phone_number;
        public string contry;
        public string date_of_birdth;
        public string organization;
        public string position;
        public string other_notes;
        public int id;
        public static int id_max;
        public Note()
        {
            this.id = id_max ;
            id_max++;
        }
        public override string ToString()
        {
            if (patronymic == "")
                patronymic = "Данные отсутствуют";
            if (date_of_birdth == "")
                date_of_birdth = "Данные отсутствуют";
            if (organization == "")
                organization = "Данные отсутствуют";
            if (position == "")
                position = "Данные отсутствуют";
            if (other_notes == "")
                other_notes = "Данные отсутствуют";

            return $"Имя: {first_name}\n" +
                $"Фамилия: {last_name}\n" +
                $"Отчество: {patronymic}\n" +
                $"Номер телефона: {phone_number}\n" +
                $"Страна: {contry}\n" +
                $"Дата рождения: {date_of_birdth}\n" +
                $"Организация: {organization}\n" +
                $"Должность: {position}\n" +
                $"Прочие заметки: {other_notes}\n";
              ;
        }

    }
}
