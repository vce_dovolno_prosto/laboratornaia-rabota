﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ConsoleApp1
{

    class Program
    {
        static void Action()
        {
            Console.WriteLine("Что вы хотите сделать:\n" +
                   "1) Создать новую запись\n" +
                   "2) Редактировать созданную запись\n" +
                   "3) Удаление созданной записи\n" +
                   "4) Просмотр созданной учетной записи\n" +
                   "5) Просмотр всех созданных учетных записей,с краткой информацией включающие создающие основные поля:\n" +
                   "a.Фамилия;\n" +
                   "b.Имя;\n" +
                   "c.Номер телефона\n" +
                   "0) Закрыть программу");

        }
        static void Add_man_context(string context, ref string context_value) // Строгие поля
        {
            bool b = false;
            bool b1 = false;
            while (b == false)
            {
                if (!b1)
                    Console.WriteLine($"Введите {context}");
                else
                    Console.WriteLine($"Введите {context}.Это поле является обязательным");
                context_value = Console.ReadLine().Trim();
                Console.Clear();
                if (context_value != "")
                    b = true;
                b1 = true;
            }
           


        }
        static void Add_man(ref List<Note> notes) // Создание новой записи 
        {
            Note man = new Note();
            Console.Clear();
            Add_man_context("Имя", ref man.first_name);
            Add_man_context("Фамилию", ref man.last_name);
            Console.WriteLine("Введите Отчество");
            man.patronymic = Console.ReadLine().Trim();
            Console.Clear();

            Console.WriteLine("Введите номер телефона(только цифры)");
            bool b = false;
            while (b == false)
            {
                b = long.TryParse(Console.ReadLine(), out man.phone_number);
                Console.Clear();
                if (b == true)
                { }
                else Console.WriteLine("Это поле является обязательным. Условие - только цифры!");
                
            }
            Add_man_context("Страну", ref man.contry);
            Console.WriteLine("Введите дату рождения");
            DateTime dtOut;
            b = false;
            string s;
            while (!b)
            {
                s = Console.ReadLine();
                if ((DateTime.TryParse(s, out dtOut)) || (s == "" ) )
                {
                    man.date_of_birdth = Convert.ToString(dtOut);
                    b = true;
                }
                
                Console.WriteLine("Введите дату рождения в формате: {0:d} или пропустите", new DateTime(2008, 1, 7));
            }           
            Console.Clear();
            Console.WriteLine("Организацию");
            man.organization = Console.ReadLine().Trim();
            Console.Clear();
            Console.WriteLine("Должность");
            man.position = Console.ReadLine().Trim();
            Console.Clear();
            Console.WriteLine("Прочие заметки");
            man.other_notes = Console.ReadLine().Trim();
            Console.Clear();
            notes.Add(man);
        }

        static void Output_profile(ref List<Note> notes) // Вывод профиля
        {
            Output_profile_all(ref notes);
            Console.WriteLine("Вывод пользователя с помощью Id");
            Console.WriteLine("Выйти в меню - команда < 0 > ");
            bool b = false;
            bool b1 = false;
            int id_output;
            while (!b)
            {
                b1 = int.TryParse(Console.ReadLine(), out id_output);
                id_output--;
                Console.Clear();

                if ((id_output < -1) || (id_output > Note.id_max - 1) || (b1 == false))
                {

                    Console.WriteLine("Пользователь с данным Id не сущесвует");
                    Console.WriteLine("Выйти в меню - команда < 0 > ");
                    Output_profile_all(ref notes);
                }
                else if (id_output == -1)
                    b = true;
                else
                {                    
                    Console.WriteLine(notes[id_output]);
                    b = true;

                }
            }


        }
        static void Output_profile_all(ref List<Note> notes) // вывод всех профилей с кратко информацией
        {
            foreach ( Note man in notes)
            {
                Console.WriteLine(man.id + 1 + " . " +  $"Имя: {man.first_name}\n" +
                    $"Фамилия: {man.last_name}\n" +
                    $"Номер телефона: {man.phone_number}\n");
            }
        }
        static void Edit_profile(ref List<Note> notes) // Редактирование профиля
        {
            
            
            bool b = false;
            bool b1 = false;
            int id_edit;
            while (!b)
            {
                Console.Clear();
                Console.WriteLine("Редактировие пользователя с помощью Id");
                Console.WriteLine("Выйти в меню - команда < 0 > ");
                Output_profile_all(ref notes);
                b1 = int.TryParse(Console.ReadLine(), out id_edit);
                id_edit--;
                Console.Clear();

                if ((id_edit < -1) || (id_edit > Note.id_max - 1) || (b1 == false))
                {
                   

                    Console.WriteLine("Пользователь с данным Id не сущесвует");
                    Console.WriteLine("Выйти в меню - команда < 0 > ");
                    
                    
                }
                else if (id_edit == -1)
                    b = true;
                else
                {
                   Note man = notes[id_edit];
                    Console.Clear();
                    bool b2 = false ;
                    while (!b2)
                    {
                        Console.WriteLine(man);
                        Console.WriteLine("Что именно вы хотите поменять? \n");
                        Console.WriteLine($"1.Имя\n" +
                            $"2. Фамилию\n" +
                            $"3. Отчество\n" +
                            $"4. Номер телефона\n" +
                            $"5. Страну\n" +
                            $"6. Дату рождения\n" +
                            $"7. Организацию\n" +
                            $"8. Должность\n" +
                            $"9. Прочие заметки\n");

                        switch (Console.ReadLine())
                        {
                            case "1":
                                Add_man_context("Имя", ref man.first_name);
                                b2 = true;
                                break;
                            case "2":
                                Add_man_context("Фамилию", ref man.last_name);
                                b2 = true;
                                break;
                            case "3":
                                Console.WriteLine("Введите Отчество");
                                man.patronymic = Console.ReadLine().Trim();
                                b2 = true;
                                break;
                            case "4":
                                Console.WriteLine("Введие номер телефона(только цифры)");
                                b = false;
                                while (b == false)
                                {
                                    b = long.TryParse(Console.ReadLine(), out man.phone_number);
                                    Console.Clear();
                                    if (b == true)
                                    { }
                                    else Console.WriteLine("Это поле является обязательным. Условие - только цифры!");

                                }
                                b2 = true;
                                break;
                            case "5":
                                Add_man_context("Страну", ref man.contry);
                                b2 = true;
                                break;
                            case "6":
                                b2 = true;
                                Console.WriteLine("Введите дату рождения");
                                DateTime dtOut;
                                b = false;
                                string s;
                                while (!b)
                                {
                                    s = Console.ReadLine();
                                    if ((DateTime.TryParse(s, out dtOut)) || (s == ""))
                                    {
                                        man.date_of_birdth = Convert.ToString(dtOut);
                                        b = true;
                                    }

                                    Console.WriteLine("Введите дату рождения в формате: {0:d} или пропустите", new DateTime(2008, 1, 7));
                                }
                                break;
                            case "7":
                                Console.WriteLine("Организацию");
                                man.organization = Console.ReadLine().Trim();
                                b2 = true;
                                break;
                            case "8":
                                Console.WriteLine("Должность");
                                man.position = Console.ReadLine().Trim();
                                b2 = true;
                                break;
                            case "9":
                                b2 = true;
                                Console.WriteLine("Прочие заметки");
                                man.other_notes = Console.ReadLine().Trim();
                                break;
                            default:
                                break;

                        }
                        Console.Clear();
                        
                    }
                    

                }
            }


        }

        static void Delete_profile(ref List<Note> notes) // Удаления созданной записи 
        {
            Output_profile_all(ref notes);
            Console.WriteLine("Удаление пользователя с помощью Id");
            Console.WriteLine("Выйти в меню - команда < 0 > ");
            bool b = false;
            bool b1 = false;
            int id_delete;

            while (!b)
            {
                b1 = int.TryParse(Console.ReadLine(), out id_delete);
                id_delete--;
                Console.Clear();

                if ((id_delete < -1) || (id_delete > Note.id_max - 1) || (b1 == false) ) 
                {
                    
                    Console.WriteLine("Пользователь с данным Id не сущесвует");
                    Console.WriteLine("Выйти в меню - команда < 0 > ");
                    Output_profile_all(ref notes);
                }
                else if (id_delete == -1)
                    b = true;
                else
                {
                    
                    notes.RemoveAt(id_delete);
                    Note.id_max--;
                    for (int j = id_delete ; j <= Note.id_max - 1; j++)
                    {
                        notes[j].id--;
                    }
                    
                   
                    
                    Console.WriteLine("Пользователь удалён");
                    b = true;

                }
            }
           
              
        }


        static void Viewing(ref List<Note> notes) // Просмотр всех учетных записей 
        {
            foreach(Note man in notes)
            {
                Console.WriteLine($"Имя: {man.first_name}\n" +
                    $"Фамилия: {man.last_name}\n" +
                    $"Номер телефона: {man.phone_number}\n");
               

            }

        }
        

        
        static void Main(string[] args)
        {
            List<Note> all_man = new List<Note>();
            
            int j;
            do
            {
                bool b;
                Console.WriteLine("Здраствуйте!");
                Action();
                b = int.TryParse(Console.ReadLine(), out j);
                Console.Clear();
                if (!b)
                { Console.WriteLine("Код функции не найден"); j = 1; }
                else
                {
                    switch (j)
                    {
                        case 1:
                            Add_man(ref all_man);
                            break;
                        case 2:
                            Edit_profile(ref all_man);
                            break;

                        case 3:
                            Delete_profile(ref all_man);
                            break;
                        case 4:
                            Output_profile(ref all_man);
                            break;
                        case 5:
                            Viewing(ref all_man);
                            break;
                        case 0:
                            break;
                        default:
                            Console.WriteLine("Код функции не найден");
                            break;


                    }
                }
               
            } while (j != 0);


        }
    }
}
